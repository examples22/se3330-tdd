﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIS
{
    public class Student
    {
        private const int ADULT_AGE = 18;
        public DateTime birth_date { get; set; }

        public bool IsMinor()
        {
            return Age() < ADULT_AGE;
        }

        private int Age()
        {
            DateTime current = DateTime.Now;
            int age = current.Year - birth_date.Year;
            if (current.Month < birth_date.Month || (current.Month == birth_date.Month && current.Day < birth_date.Day))
                age--;
            return age;
        }
    }
}
