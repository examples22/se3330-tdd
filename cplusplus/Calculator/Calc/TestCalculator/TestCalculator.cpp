#include "pch.h"
#include "CppUnitTest.h"
#include "../Calc/Calculator.h"
#include "../Calc/Calculator.cpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace TestCalculator
{
	TEST_CLASS(TestCalculator)
	{
	public:
		
		TEST_METHOD(TestAddition)
		{
			Calculator calc;
			Assert::AreEqual(10.0, calc.Add(2, 8));
		}

		TEST_METHOD(TestAverage_Normal)
		{
			Calculator calc;
			double input[10] = { 1,2,3,4,5 };
			int size = 5;
			Assert::AreEqual(3.0, calc.Average(input, size));
		}

		TEST_METHOD(TestAverage_EmptyArray)
		{
			Calculator calc;
			double input[10];
			int size = 0;
			Assert::AreEqual(0.0, calc.Average(input, size));
		}

	};
}
