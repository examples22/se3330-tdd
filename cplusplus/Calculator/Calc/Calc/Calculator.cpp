#include "Calculator.h"

double Calculator::Add(double num1, double num2)
{
	return num1 + num2;
}

double Calculator::Average(double nums[], int size)
{
	if (size == 0)
		return 0;
	double total = 0;
	for (int i = 0; i < size; i++)
		total += nums[i];
	return total/size;
}
